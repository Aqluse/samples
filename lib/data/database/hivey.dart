import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'abstracts/database.dart';

class Hivey extends Database<BoxBase> {
  Hivey(
      {

      /// Use to put `openBox` calls there,
      /// if you want a little speed-up on first read
      VoidCallback? onInitializeCompleted})
      : super(onInitializeCompleted: onInitializeCompleted);

  Future<void>? _initializationPending;

  @override
  Future<void> init() async {
    // No Future if initialized
    await _initializationPending;
    if (initialized) return;
    await (_initializationPending = Future.microtask(Hive.initFlutter));
    super.init();
    return _initializationPending = null;
  }

  void registerAdapter<T>(TypeAdapter<T> adapter) =>
      Hive.registerAdapter<T>(adapter);

  @override
  Future<BoxBase<T>> getTypeStore<T>(String holderId, {bool lazy = false}) {
    return lazy ? getLazyBox<T>(holderId) : getBox<T>(holderId);
  }

  Future<Box<T>> getBox<T>(String name) async {
    await init();
    return Hive.isBoxOpen(name)
        ? Hive.box<T>(name)
        : await Hive.openBox<T>(name);
  }

  Future<LazyBox<T>> getLazyBox<T>(String name) async {
    await init();
    return Hive.isBoxOpen(name)
        ? Hive.lazyBox<T>(name)
        : await Hive.openLazyBox<T>(name);
  }

  @override
  Future<void> close() {
    return Hive.close();
  }

  // TODO: May become useful in a future
/*static void typedBoxSwitcher<T>(BoxBase<T> box,
      {void Function(Box<T>) onBox,
      void Function(LazyBox<T>) onLazyBox,
      VoidCallback orElse}) {
    assert(box != null);

    if (box is Box<T>)
      onBox?.call(box);
    else if (box is LazyBox<T>)
      onLazyBox?.call(box);
    else if (orElse != null)
      orElse();
    else
      throw UnimplementedError('Box of unexpected type! [${box.runtimeType}]');
  }*/
}
