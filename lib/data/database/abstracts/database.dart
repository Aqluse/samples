import 'dart:async';

import 'package:flutter/foundation.dart';

/// Abstraction with common database functionality
/// [T] is a type of data-holder structure (e.g. table in SQL-like databases)
abstract class Database<T> {
  Database({this.onInitializeCompleted});

  final VoidCallback? onInitializeCompleted;

  @protected
  final Completer<void> _initializationCompleter = Completer<void>();

  set initialized(bool value) {
    if (value) {
      _initializationCompleter.complete();
      onInitializeCompleted?.call();
    }
  }

  bool get initialized => _initializationCompleter.isCompleted;

  /// Should call this at the end of overridden method
  @mustCallSuper
  Future<void> init() {
    initialized = true;
    return Future.value(null);
  }

  /// [U] is a data-type that data-holder contains
  Future<T> getTypeStore<U>(String holderId);

  /// Closes instance of database
  Future<void> close();
}
