/* TODO: WIP.
    Common interfaces for various data-stores (can change in future)
 */

abstract class DatabaseStoreBase<T> {}

abstract class InMemoryDatabaseStore<T> extends DatabaseStoreBase<T> {}

abstract class LazyDatabaseStore<T> extends DatabaseStoreBase<T> {}
