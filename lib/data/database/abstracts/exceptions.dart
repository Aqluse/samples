class NotFoundException implements Exception {
  const NotFoundException({this.message = 'Cannot find queried data'});

  final String message;
}
