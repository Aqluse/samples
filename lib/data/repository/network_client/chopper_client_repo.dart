import 'package:arch_components/repository.dart';
import 'package:chopper/chopper.dart';

class ChopperClientRepository extends NetworkClientRepository<ChopperClient> {
  ChopperClientRepository(final ChopperClient client) : super(client: client);
}
