import 'package:freezed_annotation/freezed_annotation.dart';

part 'common.freezed.dart';
part 'common.g.dart';

@freezed
class ErrorResponse with _$ErrorResponse {
  const ErrorResponse._();

  const factory ErrorResponse.withMessages(
      {required List<ErrorResponse> messages}) = MessagesErrorResponse;

  factory ErrorResponse.fromJson(Map<String, dynamic> json) =>
      _$ErrorResponseFromJson(json);
}

@freezed
class Message with _$Message {
  const Message._();

  const factory Message(
      {required String message, required String description}) = _Message;

  const factory Message.validation(
      {required String field,
      required String message,
      required String description}) = _ValidationMessage;

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);
}
