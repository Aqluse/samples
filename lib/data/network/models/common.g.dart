// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'common.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$MessagesErrorResponse _$_$MessagesErrorResponseFromJson(
    Map<String, dynamic> json) {
  return _$MessagesErrorResponse(
    messages: (json['messages'] as List<dynamic>)
        .map((e) => ErrorResponse.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$_$MessagesErrorResponseToJson(
        _$MessagesErrorResponse instance) =>
    <String, dynamic>{
      'messages': instance.messages,
    };

_$_Message _$_$_MessageFromJson(Map<String, dynamic> json) {
  return _$_Message(
    message: json['message'] as String,
    description: json['description'] as String,
  );
}

Map<String, dynamic> _$_$_MessageToJson(_$_Message instance) =>
    <String, dynamic>{
      'message': instance.message,
      'description': instance.description,
    };

_$_ValidationMessage _$_$_ValidationMessageFromJson(Map<String, dynamic> json) {
  return _$_ValidationMessage(
    field: json['field'] as String,
    message: json['message'] as String,
    description: json['description'] as String,
  );
}

Map<String, dynamic> _$_$_ValidationMessageToJson(
        _$_ValidationMessage instance) =>
    <String, dynamic>{
      'field': instance.field,
      'message': instance.message,
      'description': instance.description,
    };
