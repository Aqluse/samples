// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'common.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ErrorResponse _$ErrorResponseFromJson(Map<String, dynamic> json) {
  return MessagesErrorResponse.fromJson(json);
}

/// @nodoc
class _$ErrorResponseTearOff {
  const _$ErrorResponseTearOff();

  MessagesErrorResponse withMessages({required List<ErrorResponse> messages}) {
    return MessagesErrorResponse(
      messages: messages,
    );
  }

  ErrorResponse fromJson(Map<String, Object> json) {
    return ErrorResponse.fromJson(json);
  }
}

/// @nodoc
const $ErrorResponse = _$ErrorResponseTearOff();

/// @nodoc
mixin _$ErrorResponse {
  List<ErrorResponse> get messages => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ErrorResponse> messages) withMessages,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ErrorResponse> messages)? withMessages,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MessagesErrorResponse value) withMessages,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MessagesErrorResponse value)? withMessages,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ErrorResponseCopyWith<ErrorResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ErrorResponseCopyWith<$Res> {
  factory $ErrorResponseCopyWith(
          ErrorResponse value, $Res Function(ErrorResponse) then) =
      _$ErrorResponseCopyWithImpl<$Res>;
  $Res call({List<ErrorResponse> messages});
}

/// @nodoc
class _$ErrorResponseCopyWithImpl<$Res>
    implements $ErrorResponseCopyWith<$Res> {
  _$ErrorResponseCopyWithImpl(this._value, this._then);

  final ErrorResponse _value;
  // ignore: unused_field
  final $Res Function(ErrorResponse) _then;

  @override
  $Res call({
    Object? messages = freezed,
  }) {
    return _then(_value.copyWith(
      messages: messages == freezed
          ? _value.messages
          : messages // ignore: cast_nullable_to_non_nullable
              as List<ErrorResponse>,
    ));
  }
}

/// @nodoc
abstract class $MessagesErrorResponseCopyWith<$Res>
    implements $ErrorResponseCopyWith<$Res> {
  factory $MessagesErrorResponseCopyWith(MessagesErrorResponse value,
          $Res Function(MessagesErrorResponse) then) =
      _$MessagesErrorResponseCopyWithImpl<$Res>;
  @override
  $Res call({List<ErrorResponse> messages});
}

/// @nodoc
class _$MessagesErrorResponseCopyWithImpl<$Res>
    extends _$ErrorResponseCopyWithImpl<$Res>
    implements $MessagesErrorResponseCopyWith<$Res> {
  _$MessagesErrorResponseCopyWithImpl(
      MessagesErrorResponse _value, $Res Function(MessagesErrorResponse) _then)
      : super(_value, (v) => _then(v as MessagesErrorResponse));

  @override
  MessagesErrorResponse get _value => super._value as MessagesErrorResponse;

  @override
  $Res call({
    Object? messages = freezed,
  }) {
    return _then(MessagesErrorResponse(
      messages: messages == freezed
          ? _value.messages
          : messages // ignore: cast_nullable_to_non_nullable
              as List<ErrorResponse>,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$MessagesErrorResponse extends MessagesErrorResponse {
  const _$MessagesErrorResponse({required this.messages}) : super._();

  factory _$MessagesErrorResponse.fromJson(Map<String, dynamic> json) =>
      _$_$MessagesErrorResponseFromJson(json);

  @override
  final List<ErrorResponse> messages;

  @override
  String toString() {
    return 'ErrorResponse.withMessages(messages: $messages)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is MessagesErrorResponse &&
            (identical(other.messages, messages) ||
                const DeepCollectionEquality()
                    .equals(other.messages, messages)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(messages);

  @JsonKey(ignore: true)
  @override
  $MessagesErrorResponseCopyWith<MessagesErrorResponse> get copyWith =>
      _$MessagesErrorResponseCopyWithImpl<MessagesErrorResponse>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ErrorResponse> messages) withMessages,
  }) {
    return withMessages(messages);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ErrorResponse> messages)? withMessages,
    required TResult orElse(),
  }) {
    if (withMessages != null) {
      return withMessages(messages);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MessagesErrorResponse value) withMessages,
  }) {
    return withMessages(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MessagesErrorResponse value)? withMessages,
    required TResult orElse(),
  }) {
    if (withMessages != null) {
      return withMessages(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$MessagesErrorResponseToJson(this);
  }
}

abstract class MessagesErrorResponse extends ErrorResponse {
  const factory MessagesErrorResponse({required List<ErrorResponse> messages}) =
      _$MessagesErrorResponse;
  const MessagesErrorResponse._() : super._();

  factory MessagesErrorResponse.fromJson(Map<String, dynamic> json) =
      _$MessagesErrorResponse.fromJson;

  @override
  List<ErrorResponse> get messages => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $MessagesErrorResponseCopyWith<MessagesErrorResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

Message _$MessageFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType'] as String) {
    case 'default':
      return _Message.fromJson(json);
    case 'validation':
      return _ValidationMessage.fromJson(json);

    default:
      throw FallThroughError();
  }
}

/// @nodoc
class _$MessageTearOff {
  const _$MessageTearOff();

  _Message call({required String message, required String description}) {
    return _Message(
      message: message,
      description: description,
    );
  }

  _ValidationMessage validation(
      {required String field,
      required String message,
      required String description}) {
    return _ValidationMessage(
      field: field,
      message: message,
      description: description,
    );
  }

  Message fromJson(Map<String, Object> json) {
    return Message.fromJson(json);
  }
}

/// @nodoc
const $Message = _$MessageTearOff();

/// @nodoc
mixin _$Message {
  String get message => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(String message, String description) $default, {
    required TResult Function(String field, String message, String description)
        validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(String message, String description)? $default, {
    TResult Function(String field, String message, String description)?
        validation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Message value) $default, {
    required TResult Function(_ValidationMessage value) validation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Message value)? $default, {
    TResult Function(_ValidationMessage value)? validation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MessageCopyWith<Message> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageCopyWith<$Res> {
  factory $MessageCopyWith(Message value, $Res Function(Message) then) =
      _$MessageCopyWithImpl<$Res>;
  $Res call({String message, String description});
}

/// @nodoc
class _$MessageCopyWithImpl<$Res> implements $MessageCopyWith<$Res> {
  _$MessageCopyWithImpl(this._value, this._then);

  final Message _value;
  // ignore: unused_field
  final $Res Function(Message) _then;

  @override
  $Res call({
    Object? message = freezed,
    Object? description = freezed,
  }) {
    return _then(_value.copyWith(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$MessageCopyWith<$Res> implements $MessageCopyWith<$Res> {
  factory _$MessageCopyWith(_Message value, $Res Function(_Message) then) =
      __$MessageCopyWithImpl<$Res>;
  @override
  $Res call({String message, String description});
}

/// @nodoc
class __$MessageCopyWithImpl<$Res> extends _$MessageCopyWithImpl<$Res>
    implements _$MessageCopyWith<$Res> {
  __$MessageCopyWithImpl(_Message _value, $Res Function(_Message) _then)
      : super(_value, (v) => _then(v as _Message));

  @override
  _Message get _value => super._value as _Message;

  @override
  $Res call({
    Object? message = freezed,
    Object? description = freezed,
  }) {
    return _then(_Message(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Message extends _Message {
  const _$_Message({required this.message, required this.description})
      : super._();

  factory _$_Message.fromJson(Map<String, dynamic> json) =>
      _$_$_MessageFromJson(json);

  @override
  final String message;
  @override
  final String description;

  @override
  String toString() {
    return 'Message(message: $message, description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Message &&
            (identical(other.message, message) ||
                const DeepCollectionEquality()
                    .equals(other.message, message)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(message) ^
      const DeepCollectionEquality().hash(description);

  @JsonKey(ignore: true)
  @override
  _$MessageCopyWith<_Message> get copyWith =>
      __$MessageCopyWithImpl<_Message>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(String message, String description) $default, {
    required TResult Function(String field, String message, String description)
        validation,
  }) {
    return $default(message, description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(String message, String description)? $default, {
    TResult Function(String field, String message, String description)?
        validation,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(message, description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Message value) $default, {
    required TResult Function(_ValidationMessage value) validation,
  }) {
    return $default(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Message value)? $default, {
    TResult Function(_ValidationMessage value)? validation,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MessageToJson(this)..['runtimeType'] = 'default';
  }
}

abstract class _Message extends Message {
  const factory _Message(
      {required String message, required String description}) = _$_Message;
  const _Message._() : super._();

  factory _Message.fromJson(Map<String, dynamic> json) = _$_Message.fromJson;

  @override
  String get message => throw _privateConstructorUsedError;
  @override
  String get description => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MessageCopyWith<_Message> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ValidationMessageCopyWith<$Res>
    implements $MessageCopyWith<$Res> {
  factory _$ValidationMessageCopyWith(
          _ValidationMessage value, $Res Function(_ValidationMessage) then) =
      __$ValidationMessageCopyWithImpl<$Res>;
  @override
  $Res call({String field, String message, String description});
}

/// @nodoc
class __$ValidationMessageCopyWithImpl<$Res> extends _$MessageCopyWithImpl<$Res>
    implements _$ValidationMessageCopyWith<$Res> {
  __$ValidationMessageCopyWithImpl(
      _ValidationMessage _value, $Res Function(_ValidationMessage) _then)
      : super(_value, (v) => _then(v as _ValidationMessage));

  @override
  _ValidationMessage get _value => super._value as _ValidationMessage;

  @override
  $Res call({
    Object? field = freezed,
    Object? message = freezed,
    Object? description = freezed,
  }) {
    return _then(_ValidationMessage(
      field: field == freezed
          ? _value.field
          : field // ignore: cast_nullable_to_non_nullable
              as String,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_ValidationMessage extends _ValidationMessage {
  const _$_ValidationMessage(
      {required this.field, required this.message, required this.description})
      : super._();

  factory _$_ValidationMessage.fromJson(Map<String, dynamic> json) =>
      _$_$_ValidationMessageFromJson(json);

  @override
  final String field;
  @override
  final String message;
  @override
  final String description;

  @override
  String toString() {
    return 'Message.validation(field: $field, message: $message, description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ValidationMessage &&
            (identical(other.field, field) ||
                const DeepCollectionEquality().equals(other.field, field)) &&
            (identical(other.message, message) ||
                const DeepCollectionEquality()
                    .equals(other.message, message)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(field) ^
      const DeepCollectionEquality().hash(message) ^
      const DeepCollectionEquality().hash(description);

  @JsonKey(ignore: true)
  @override
  _$ValidationMessageCopyWith<_ValidationMessage> get copyWith =>
      __$ValidationMessageCopyWithImpl<_ValidationMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(String message, String description) $default, {
    required TResult Function(String field, String message, String description)
        validation,
  }) {
    return validation(field, message, description);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(String message, String description)? $default, {
    TResult Function(String field, String message, String description)?
        validation,
    required TResult orElse(),
  }) {
    if (validation != null) {
      return validation(field, message, description);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Message value) $default, {
    required TResult Function(_ValidationMessage value) validation,
  }) {
    return validation(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Message value)? $default, {
    TResult Function(_ValidationMessage value)? validation,
    required TResult orElse(),
  }) {
    if (validation != null) {
      return validation(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ValidationMessageToJson(this)..['runtimeType'] = 'validation';
  }
}

abstract class _ValidationMessage extends Message {
  const factory _ValidationMessage(
      {required String field,
      required String message,
      required String description}) = _$_ValidationMessage;
  const _ValidationMessage._() : super._();

  factory _ValidationMessage.fromJson(Map<String, dynamic> json) =
      _$_ValidationMessage.fromJson;

  String get field => throw _privateConstructorUsedError;
  @override
  String get message => throw _privateConstructorUsedError;
  @override
  String get description => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ValidationMessageCopyWith<_ValidationMessage> get copyWith =>
      throw _privateConstructorUsedError;
}
