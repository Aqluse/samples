import 'package:arch_components/network.dart';
import 'package:chopper/chopper.dart';
import 'package:meta/meta.dart';
import 'package:samples/data/repository/network_client/chopper_client_repo.dart';

abstract class ChopperNetworkService<ErrorType> extends NetworkService
    with NetworkResponseConverterMixin<Response<Object>, ErrorType> {
  const ChopperNetworkService(this.clientRepository);

  @override
  final ChopperClientRepository clientRepository;

  @protected
  @override
  T responseToDataConverter<T>(final Response<Object> response) {
    if (response.isSuccessful)
      return response.body as T;
    else
      throwOnErrorBody(response.error);
  }
}
