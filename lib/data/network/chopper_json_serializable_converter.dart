import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:collection/collection.dart';

typedef JsonFactory<T> = T Function(Map<String, Object?> json);

class NotSupportedJsonSourceException implements Exception {
  const NotSupportedJsonSourceException({
    final String? message,
  }) : message = message ?? 'Cannot convert json from non-String source';

  final String message;
}

/// Proxy converter for chopper library
/// to use json_serializable-way to convert json

class JsonSerializableConverter<ErrorType> extends JsonConverter {
  const JsonSerializableConverter(this.factories);

  final Map<Type, JsonFactory<Object?>> factories;

  Object? _decode<T>(final Object? entity) {
    if (entity is Iterable) return _decodeList<T>(entity);
    if (entity is Map<String, Object?>) return _decodeMap<T>(entity);

    return entity;
  }

  T? _decodeMap<T>(final Map<String, Object?> values) {
    /// Get jsonFactory using Type parameters
    /// if not found or invalid, throw error or return null
    final jsonFactory = factories[T];
    if (jsonFactory is! JsonFactory<T>) {
      assert(false, 'Cannot find suitable serializer factory');
      return null;
    }
    return jsonFactory(values);
  }

  List<T> _decodeList<T>(final Iterable<Object?> values) =>
      values.whereNotNull().map<T>((final v) => _decode<T>(v) as T).toList();

  @override
  Response<BodyType> convertResponse<BodyType, PlainType>(
    final Response<Object?> response,
  ) {
    final body = response.body;

    if (body is String) {
      return response.copyWith<BodyType>(
        body: body.isEmpty
            ? null
            : _decode<PlainType>(jsonDecode(body)) as BodyType,
      );
    } else
      throw const NotSupportedJsonSourceException();
  }

  @override
  Response<Object?> convertError<BodyType, PlainType>(
    final Response<Object?> response,
  ) {
    final errorBody = response.error;

    if (errorBody is String) {
      return response.copyWith<BodyType>(
        bodyError: errorBody.isEmpty
            ? null
            : _decode<ErrorType>(jsonDecode(errorBody)),
      );
    } else
      throw const NotSupportedJsonSourceException();
  }
}
