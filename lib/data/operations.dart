abstract class Creatable<T> {
  Future<T> create();
}

abstract class Fetchable<T> {
  Future<T> fetch();
}

abstract class Updatable<T> {
  Future<T> update();
}

abstract class Deletable<T> {
  Future<T> delete();
}

abstract class Insertable<T> {
  Future<T> insert();
}

abstract class Removable<T> {
  Future<T> remove();
}

abstract class Addable<T> {
  Future<T> add();
}

abstract class ParameterizedCreatable<T, P> {
  Future<T> create({required P params});
}

abstract class ParameterizedFetchable<T, P> {
  Future<T> fetch({required P params});
}

abstract class ParameterizedUpdatable<T, P> {
  Future<T> update({required P params});
}

abstract class ParameterizedDeletable<T, P> {
  Future<T> delete({required P params});
}

abstract class ParameterizedInsertable<T, P> {
  Future<T> insert({required P params});
}

abstract class ParameterizedRemovable<T, P> {
  Future<T> remove({required P params});
}

abstract class ParameterizedAddable<T, P> {
  Future<T> add({required P params});
}

/// Read = fetch
abstract class CRUD<T>
    implements Creatable<T>, Fetchable<T>, Updatable<T>, Deletable<T> {}

/// Read = fetch
abstract class ParameterizedCRUD<T, CP, FP, UP, DP>
    implements
        ParameterizedCreatable<T, CP>,
        ParameterizedFetchable<T, FP>,
        ParameterizedUpdatable<T, UP>,
        ParameterizedDeletable<T, DP> {}

/// Allows don't use parameters with operations that usually
/// can implicitly get required parameters from locally cached model.
/// Read = fetch
abstract class ParameterizedMutatorCRUD<T, CP, UP>
    implements
        ParameterizedCreatable<T, CP>,
        Fetchable<T>,
        ParameterizedUpdatable<T, UP>,
        Deletable<T> {}

abstract class FIRA<T>
    implements Fetchable<T>, Insertable<T>, Removable<T>, Addable<T> {}

/// Read = fetch
abstract class ParameterizedFIRA<T, FP, IP, RP, AP, SP>
    implements
        ParameterizedFetchable<T, FP>,
        ParameterizedInsertable<T, IP>,
        ParameterizedRemovable<T, RP>,
        ParameterizedAddable<T, AP> {}

/// Allows don't use parameters with operations that usually
/// can implicitly get required parameters from locally cached model.
abstract class ParameterizedMutatorFIRA<T, FP, IP, RP, AP, SP>
    implements
        Fetchable<T>,
        ParameterizedInsertable<T, IP>,
        ParameterizedRemovable<T, RP>,
        ParameterizedAddable<T, AP> {}
