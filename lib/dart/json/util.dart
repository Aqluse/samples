import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'util.freezed.dart';

@freezed
class Json with _$Json {
  const Json._();

  const factory Json.object(Map<String, dynamic> value) = _JsonObject;
  const factory Json.list(List<dynamic> values) = _JsonList;
  const factory Json.value(Object? value) = _JsonValue;

  static final Exception defaultOrElseException =
      Exception('Unexpected json type given as a value');

  factory Json.from(dynamic source) {
    final value = source is! String ? source : jsonDecode(source);

    if (value is Map<String, dynamic>) {
      return Json.object(value);
    } else if (value is List<dynamic>) {
      return Json.list(value);
    } else {
      return Json.value(value);
    }
  }

  dynamic get data {
    return when(
        object: (value) => value,
        list: (values) => values,
        value: (value) => value);
  }
}
