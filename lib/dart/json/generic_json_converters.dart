import 'package:collection/collection.dart';
import 'package:json_annotation/json_annotation.dart';

class GenericJsonConverter<T, S> extends JsonConverter<T, S> {
  const GenericJsonConverter({
    required this.map,
    final T Function()? defaultObjectProvider,
    final S Function()? defaultJsonProvider,
  })  : defaultObjectProvider = defaultObjectProvider ?? _throwUnimplemented,
        defaultJsonProvider = defaultJsonProvider ?? _throwUnimplemented;

  final Map<S, T> map;
  final T Function() defaultObjectProvider;
  final S Function() defaultJsonProvider;

  @override
  T fromJson(final S json) =>
      json is T ? json : (map[json] ?? defaultObjectProvider());

  @override
  S toJson(final T object) {
    if (object is S)
      return object;
    else {
      return map.entries
              .firstWhereOrNull(
                (final element) => element.value == object,
              )
              ?.key ??
          defaultJsonProvider();
    }
  }

  static Never _throwUnimplemented() => throw UnimplementedError();
}

class ListConverter<T> extends JsonConverter<List<T>, Iterable<Object?>?> {
  const ListConverter({required this.itemConverter});

  final T? Function(Object? json) itemConverter;

  @override
  List<T> fromJson(final Iterable<Object?>? json) =>
      json?.map(itemConverter).whereType<T>().toList() ?? const [];

  @override
  Iterable<Object?>? toJson(final List<T> object) =>
      object.map((final e) => (e as dynamic).toJson());
}
