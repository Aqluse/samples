// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'util.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$JsonTearOff {
  const _$JsonTearOff();

  _JsonObject object(Map<String, dynamic> value) {
    return _JsonObject(
      value,
    );
  }

  _JsonList list(List<dynamic> values) {
    return _JsonList(
      values,
    );
  }

  _JsonValue value(Object? value) {
    return _JsonValue(
      value,
    );
  }
}

/// @nodoc
const $Json = _$JsonTearOff();

/// @nodoc
mixin _$Json {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> value) object,
    required TResult Function(List<dynamic> values) list,
    required TResult Function(Object? value) value,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> value)? object,
    TResult Function(List<dynamic> values)? list,
    TResult Function(Object? value)? value,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_JsonObject value) object,
    required TResult Function(_JsonList value) list,
    required TResult Function(_JsonValue value) value,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_JsonObject value)? object,
    TResult Function(_JsonList value)? list,
    TResult Function(_JsonValue value)? value,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $JsonCopyWith<$Res> {
  factory $JsonCopyWith(Json value, $Res Function(Json) then) =
      _$JsonCopyWithImpl<$Res>;
}

/// @nodoc
class _$JsonCopyWithImpl<$Res> implements $JsonCopyWith<$Res> {
  _$JsonCopyWithImpl(this._value, this._then);

  final Json _value;
  // ignore: unused_field
  final $Res Function(Json) _then;
}

/// @nodoc
abstract class _$JsonObjectCopyWith<$Res> {
  factory _$JsonObjectCopyWith(
          _JsonObject value, $Res Function(_JsonObject) then) =
      __$JsonObjectCopyWithImpl<$Res>;
  $Res call({Map<String, dynamic> value});
}

/// @nodoc
class __$JsonObjectCopyWithImpl<$Res> extends _$JsonCopyWithImpl<$Res>
    implements _$JsonObjectCopyWith<$Res> {
  __$JsonObjectCopyWithImpl(
      _JsonObject _value, $Res Function(_JsonObject) _then)
      : super(_value, (v) => _then(v as _JsonObject));

  @override
  _JsonObject get _value => super._value as _JsonObject;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_JsonObject(
      value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
    ));
  }
}

/// @nodoc
class _$_JsonObject extends _JsonObject {
  const _$_JsonObject(this.value) : super._();

  @override
  final Map<String, dynamic> value;

  @override
  String toString() {
    return 'Json.object(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _JsonObject &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(value);

  @JsonKey(ignore: true)
  @override
  _$JsonObjectCopyWith<_JsonObject> get copyWith =>
      __$JsonObjectCopyWithImpl<_JsonObject>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> value) object,
    required TResult Function(List<dynamic> values) list,
    required TResult Function(Object? value) value,
  }) {
    return object(this.value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> value)? object,
    TResult Function(List<dynamic> values)? list,
    TResult Function(Object? value)? value,
    required TResult orElse(),
  }) {
    if (object != null) {
      return object(this.value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_JsonObject value) object,
    required TResult Function(_JsonList value) list,
    required TResult Function(_JsonValue value) value,
  }) {
    return object(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_JsonObject value)? object,
    TResult Function(_JsonList value)? list,
    TResult Function(_JsonValue value)? value,
    required TResult orElse(),
  }) {
    if (object != null) {
      return object(this);
    }
    return orElse();
  }
}

abstract class _JsonObject extends Json {
  const factory _JsonObject(Map<String, dynamic> value) = _$_JsonObject;
  const _JsonObject._() : super._();

  Map<String, dynamic> get value => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$JsonObjectCopyWith<_JsonObject> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$JsonListCopyWith<$Res> {
  factory _$JsonListCopyWith(_JsonList value, $Res Function(_JsonList) then) =
      __$JsonListCopyWithImpl<$Res>;
  $Res call({List<dynamic> values});
}

/// @nodoc
class __$JsonListCopyWithImpl<$Res> extends _$JsonCopyWithImpl<$Res>
    implements _$JsonListCopyWith<$Res> {
  __$JsonListCopyWithImpl(_JsonList _value, $Res Function(_JsonList) _then)
      : super(_value, (v) => _then(v as _JsonList));

  @override
  _JsonList get _value => super._value as _JsonList;

  @override
  $Res call({
    Object? values = freezed,
  }) {
    return _then(_JsonList(
      values == freezed
          ? _value.values
          : values // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc
class _$_JsonList extends _JsonList {
  const _$_JsonList(this.values) : super._();

  @override
  final List<dynamic> values;

  @override
  String toString() {
    return 'Json.list(values: $values)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _JsonList &&
            (identical(other.values, values) ||
                const DeepCollectionEquality().equals(other.values, values)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(values);

  @JsonKey(ignore: true)
  @override
  _$JsonListCopyWith<_JsonList> get copyWith =>
      __$JsonListCopyWithImpl<_JsonList>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> value) object,
    required TResult Function(List<dynamic> values) list,
    required TResult Function(Object? value) value,
  }) {
    return list(values);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> value)? object,
    TResult Function(List<dynamic> values)? list,
    TResult Function(Object? value)? value,
    required TResult orElse(),
  }) {
    if (list != null) {
      return list(values);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_JsonObject value) object,
    required TResult Function(_JsonList value) list,
    required TResult Function(_JsonValue value) value,
  }) {
    return list(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_JsonObject value)? object,
    TResult Function(_JsonList value)? list,
    TResult Function(_JsonValue value)? value,
    required TResult orElse(),
  }) {
    if (list != null) {
      return list(this);
    }
    return orElse();
  }
}

abstract class _JsonList extends Json {
  const factory _JsonList(List<dynamic> values) = _$_JsonList;
  const _JsonList._() : super._();

  List<dynamic> get values => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$JsonListCopyWith<_JsonList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$JsonValueCopyWith<$Res> {
  factory _$JsonValueCopyWith(
          _JsonValue value, $Res Function(_JsonValue) then) =
      __$JsonValueCopyWithImpl<$Res>;
  $Res call({Object? value});
}

/// @nodoc
class __$JsonValueCopyWithImpl<$Res> extends _$JsonCopyWithImpl<$Res>
    implements _$JsonValueCopyWith<$Res> {
  __$JsonValueCopyWithImpl(_JsonValue _value, $Res Function(_JsonValue) _then)
      : super(_value, (v) => _then(v as _JsonValue));

  @override
  _JsonValue get _value => super._value as _JsonValue;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_JsonValue(
      value == freezed ? _value.value : value,
    ));
  }
}

/// @nodoc
class _$_JsonValue extends _JsonValue {
  const _$_JsonValue(this.value) : super._();

  @override
  final Object? value;

  @override
  String toString() {
    return 'Json.value(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _JsonValue &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(value);

  @JsonKey(ignore: true)
  @override
  _$JsonValueCopyWith<_JsonValue> get copyWith =>
      __$JsonValueCopyWithImpl<_JsonValue>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> value) object,
    required TResult Function(List<dynamic> values) list,
    required TResult Function(Object? value) value,
  }) {
    return value(this.value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> value)? object,
    TResult Function(List<dynamic> values)? list,
    TResult Function(Object? value)? value,
    required TResult orElse(),
  }) {
    if (value != null) {
      return value(this.value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_JsonObject value) object,
    required TResult Function(_JsonList value) list,
    required TResult Function(_JsonValue value) value,
  }) {
    return value(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_JsonObject value)? object,
    TResult Function(_JsonList value)? list,
    TResult Function(_JsonValue value)? value,
    required TResult orElse(),
  }) {
    if (value != null) {
      return value(this);
    }
    return orElse();
  }
}

abstract class _JsonValue extends Json {
  const factory _JsonValue(Object? value) = _$_JsonValue;
  const _JsonValue._() : super._();

  Object? get value => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$JsonValueCopyWith<_JsonValue> get copyWith =>
      throw _privateConstructorUsedError;
}
