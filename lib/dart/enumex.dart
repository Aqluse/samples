// TODO: Delete, if ALL functionality covered by Dart SDK's enhanced enum.

/// Allows to have concise written mapping from [K] type,
/// which is not necessarily is enum-class,
/// to a value of any [V] type
abstract class EnumEx<K, V> {
  EnumEx(this.map);

  final Map<K, V> map;
  late final Map<V, K> reverseMap =
      map.map((key, value) => MapEntry(value, key));

  V operator [](K key) => map[key]!;

  V byKey(K key) => _internalByKey<K, V>(map, key)!;

  K byValue(V value) => _internalByKey<V, K>(reverseMap, value)!;

  V? maybeByKey(K key) => _internalByKey<K, V>(map, key);

  K? maybeByValue(V value) => _internalByKey<V, K>(reverseMap, value);

  Value? _internalByKey<Key, Value>(Map<Key, Value> map, Key key) => map[key];
}

/// Example of use
enum Fruit { apple, orange }

const Map<Fruit, String> _fruitsMap = <Fruit, String>{
  Fruit.apple: 'Apple',
  Fruit.orange: 'Orange'
};

EnumEx<Fruit, String> get Fruits => FruitsEnumEx.I;

class FruitsEnumEx extends EnumEx<Fruit, String> {
  static late FruitsEnumEx I = FruitsEnumEx._(_fruitsMap);
  FruitsEnumEx._(Map<Fruit, String> map) : super(map);
}

// Or more simpler variant if enum to value mapping needed only.
// Switch-case is for compile-time guarantee that every case is handled by every mapper extension on certain enum, otherwise it warns.

const Map<Fruit, String> _map = <Fruit, String>{
  Fruit.apple: 'Apple',
  Fruit.orange: 'Orange'
};

extension KeyEx on Fruit {
  String get value {
    switch (this) {
      case Fruit.apple:
      case Fruit.orange:
        return _map[this]!;
    }
  }
}
