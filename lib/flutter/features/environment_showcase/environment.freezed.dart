// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'environment.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$EnvironmentTearOff {
  const _$EnvironmentTearOff();

  _Environment call({required OperatingSystem os, required Urls urls}) {
    return _Environment(
      os: os,
      urls: urls,
    );
  }
}

/// @nodoc
const $Environment = _$EnvironmentTearOff();

/// @nodoc
mixin _$Environment {
  OperatingSystem get os => throw _privateConstructorUsedError;
  Urls get urls => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $EnvironmentCopyWith<Environment> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EnvironmentCopyWith<$Res> {
  factory $EnvironmentCopyWith(
          Environment value, $Res Function(Environment) then) =
      _$EnvironmentCopyWithImpl<$Res>;
  $Res call({OperatingSystem os, Urls urls});

  $UrlsCopyWith<$Res> get urls;
}

/// @nodoc
class _$EnvironmentCopyWithImpl<$Res> implements $EnvironmentCopyWith<$Res> {
  _$EnvironmentCopyWithImpl(this._value, this._then);

  final Environment _value;
  // ignore: unused_field
  final $Res Function(Environment) _then;

  @override
  $Res call({
    Object? os = freezed,
    Object? urls = freezed,
  }) {
    return _then(_value.copyWith(
      os: os == freezed
          ? _value.os
          : os // ignore: cast_nullable_to_non_nullable
              as OperatingSystem,
      urls: urls == freezed
          ? _value.urls
          : urls // ignore: cast_nullable_to_non_nullable
              as Urls,
    ));
  }

  @override
  $UrlsCopyWith<$Res> get urls {
    return $UrlsCopyWith<$Res>(_value.urls, (value) {
      return _then(_value.copyWith(urls: value));
    });
  }
}

/// @nodoc
abstract class _$EnvironmentCopyWith<$Res>
    implements $EnvironmentCopyWith<$Res> {
  factory _$EnvironmentCopyWith(
          _Environment value, $Res Function(_Environment) then) =
      __$EnvironmentCopyWithImpl<$Res>;
  @override
  $Res call({OperatingSystem os, Urls urls});

  @override
  $UrlsCopyWith<$Res> get urls;
}

/// @nodoc
class __$EnvironmentCopyWithImpl<$Res> extends _$EnvironmentCopyWithImpl<$Res>
    implements _$EnvironmentCopyWith<$Res> {
  __$EnvironmentCopyWithImpl(
      _Environment _value, $Res Function(_Environment) _then)
      : super(_value, (v) => _then(v as _Environment));

  @override
  _Environment get _value => super._value as _Environment;

  @override
  $Res call({
    Object? os = freezed,
    Object? urls = freezed,
  }) {
    return _then(_Environment(
      os: os == freezed
          ? _value.os
          : os // ignore: cast_nullable_to_non_nullable
              as OperatingSystem,
      urls: urls == freezed
          ? _value.urls
          : urls // ignore: cast_nullable_to_non_nullable
              as Urls,
    ));
  }
}

/// @nodoc
class _$_Environment extends _Environment {
  const _$_Environment({required this.os, required this.urls}) : super._();

  @override
  final OperatingSystem os;
  @override
  final Urls urls;

  @override
  String toString() {
    return 'Environment(os: $os, urls: $urls)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Environment &&
            (identical(other.os, os) ||
                const DeepCollectionEquality().equals(other.os, os)) &&
            (identical(other.urls, urls) ||
                const DeepCollectionEquality().equals(other.urls, urls)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(os) ^
      const DeepCollectionEquality().hash(urls);

  @JsonKey(ignore: true)
  @override
  _$EnvironmentCopyWith<_Environment> get copyWith =>
      __$EnvironmentCopyWithImpl<_Environment>(this, _$identity);
}

abstract class _Environment extends Environment {
  const factory _Environment(
      {required OperatingSystem os, required Urls urls}) = _$_Environment;
  const _Environment._() : super._();

  @override
  OperatingSystem get os => throw _privateConstructorUsedError;
  @override
  Urls get urls => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$EnvironmentCopyWith<_Environment> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
class _$UrlsTearOff {
  const _$UrlsTearOff();

  _Urls call({required String apiBase}) {
    return _Urls(
      apiBase: apiBase,
    );
  }
}

/// @nodoc
const $Urls = _$UrlsTearOff();

/// @nodoc
mixin _$Urls {
  String get apiBase => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UrlsCopyWith<Urls> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UrlsCopyWith<$Res> {
  factory $UrlsCopyWith(Urls value, $Res Function(Urls) then) =
      _$UrlsCopyWithImpl<$Res>;
  $Res call({String apiBase});
}

/// @nodoc
class _$UrlsCopyWithImpl<$Res> implements $UrlsCopyWith<$Res> {
  _$UrlsCopyWithImpl(this._value, this._then);

  final Urls _value;
  // ignore: unused_field
  final $Res Function(Urls) _then;

  @override
  $Res call({
    Object? apiBase = freezed,
  }) {
    return _then(_value.copyWith(
      apiBase: apiBase == freezed
          ? _value.apiBase
          : apiBase // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$UrlsCopyWith<$Res> implements $UrlsCopyWith<$Res> {
  factory _$UrlsCopyWith(_Urls value, $Res Function(_Urls) then) =
      __$UrlsCopyWithImpl<$Res>;
  @override
  $Res call({String apiBase});
}

/// @nodoc
class __$UrlsCopyWithImpl<$Res> extends _$UrlsCopyWithImpl<$Res>
    implements _$UrlsCopyWith<$Res> {
  __$UrlsCopyWithImpl(_Urls _value, $Res Function(_Urls) _then)
      : super(_value, (v) => _then(v as _Urls));

  @override
  _Urls get _value => super._value as _Urls;

  @override
  $Res call({
    Object? apiBase = freezed,
  }) {
    return _then(_Urls(
      apiBase: apiBase == freezed
          ? _value.apiBase
          : apiBase // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
class _$_Urls extends _Urls {
  const _$_Urls({required this.apiBase}) : super._();

  @override
  final String apiBase;

  @override
  String toString() {
    return 'Urls(apiBase: $apiBase)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Urls &&
            (identical(other.apiBase, apiBase) ||
                const DeepCollectionEquality().equals(other.apiBase, apiBase)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(apiBase);

  @JsonKey(ignore: true)
  @override
  _$UrlsCopyWith<_Urls> get copyWith =>
      __$UrlsCopyWithImpl<_Urls>(this, _$identity);
}

abstract class _Urls extends Urls {
  const factory _Urls({required String apiBase}) = _$_Urls;
  const _Urls._() : super._();

  @override
  String get apiBase => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$UrlsCopyWith<_Urls> get copyWith => throw _privateConstructorUsedError;
}
