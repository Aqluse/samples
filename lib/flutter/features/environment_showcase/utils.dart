import 'enums.dart';

const String defaultEmptyValue = '';

T? envVarObjectByKey<T>(EnvVar variableKey,
    {required T Function(String envValue) valueConverter}) {
  final envValue = String.fromEnvironment(EnvVars[EnvVar.platform]);
  return envValue == defaultEmptyValue ? null : valueConverter(envValue);
}
