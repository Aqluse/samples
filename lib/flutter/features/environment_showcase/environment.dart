import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:platform_info/platform_info.dart';

import 'enums.dart';
import 'utils.dart';

part 'environment.freezed.dart';

Environment get env => Environment.read();

@freezed
class Environment with _$Environment {
  const Environment._();

  const factory Environment({required OperatingSystem os, required Urls urls}) =
      _Environment;

  factory Environment.read() {
    //return _fakeEnv();

    final os = envVarObjectByKey(EnvVar.platform,
        valueConverter: (envValue) => Platforms.byValue(envValue));
    final apiBase = envVarObjectByKey(EnvVar.urlApiBase,
        valueConverter: (envValue) => envValue);
    return Environment(
        os: os ?? platform.operatingSystem,
        urls: Urls(apiBase: apiBase ?? 'https://gugolllllll.ru/api/v1'));
  }
}

/*
Environment _fakeEnv() {
  return const Environment(
      os: OperatingSystem.iOS
  );
}
*/

@freezed
class Urls with _$Urls {
  const Urls._();

  const factory Urls({required String apiBase}) = _Urls;
}
