import 'package:platform_info/platform_info.dart';
import 'package:samples/dart/enumex.dart';

enum EnvVar { platform, urlApiBase }

Map<EnvVar, String> _envVariablesMap = <EnvVar, String>{
  EnvVar.platform: 'PLATFORM',
  EnvVar.urlApiBase: 'URL_API_BASE'
};

EnumEx<EnvVar, String> get EnvVars => EnvVarsEnumEx.I;

class EnvVarsEnumEx extends EnumEx<EnvVar, String> {
  static late EnvVarsEnumEx I = EnvVarsEnumEx._(_envVariablesMap);
  EnvVarsEnumEx._(Map<EnvVar, String> map) : super(map);
}

Map<OperatingSystem, String> _platformsMap = <OperatingSystem, String>{
  OperatingSystem.linux: 'linux',
  OperatingSystem.macOS: 'macos',
  OperatingSystem.windows: 'windows',
  OperatingSystem.android: 'android',
  OperatingSystem.iOS: 'ios',
  OperatingSystem.fuchsia: 'fuchsia'
};

EnumEx<OperatingSystem, String> get Platforms => OperatingSystemsEnumEx.I;

class OperatingSystemsEnumEx extends EnumEx<OperatingSystem, String> {
  static late OperatingSystemsEnumEx I =
      OperatingSystemsEnumEx._(_platformsMap);
  OperatingSystemsEnumEx._(Map<OperatingSystem, String> map) : super(map);
}
