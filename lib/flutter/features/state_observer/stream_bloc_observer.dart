import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

class CubitCreate {
  const CubitCreate();
}

class BlocEvent {
  const BlocEvent({required this.bloc, required this.event});

  final Bloc bloc;
  final Object event;
}

class CubitChange {
  const CubitChange({required this.cubit, required this.change});

  final Cubit cubit;
  final Change change;
}

class BlocTransition {
  const BlocTransition({required this.bloc, required this.transition});

  final Bloc bloc;
  final Transition transition;
}

class CubitError {
  const CubitError(
      {required this.cubit, required this.error, required this.stackTrace});

  final Cubit cubit;
  final Object error;
  final StackTrace stackTrace;
}

class CubitClose {
  const CubitClose();
}

class StreamBlocObserver extends BlocObserver {
  // ignore: close_sinks
  final StreamController<CubitCreate> _onCreateController =
      StreamController<CubitCreate>();
  Stream<CubitCreate> get onCreateStream => _onCreateController.stream;

  // ignore: close_sinks
  final StreamController<BlocEvent> _onEventController =
      StreamController<BlocEvent>();
  Stream<BlocEvent> get onEventStream => _onEventController.stream;

  // ignore: close_sinks
  final StreamController<CubitChange> _onChangeController =
      StreamController<CubitChange>();
  Stream<CubitChange> get onChangeStream => _onChangeController.stream;

  // ignore: close_sinks
  final StreamController<BlocTransition> _onTransitionController =
      StreamController<BlocTransition>();
  Stream<BlocTransition> get onTransitionStream =>
      _onTransitionController.stream;

  // ignore: close_sinks
  final StreamController<CubitError> _onErrorController =
      StreamController<CubitError>();
  Stream<CubitError> get onErrorStream => _onErrorController.stream;

  // ignore: close_sinks
  final StreamController<CubitClose> _onCloseController =
      StreamController<CubitClose>();
  Stream<CubitClose> get onCloseStream => _onCloseController.stream;

  List<StreamController> get _controllers => <StreamController>[
        _onCreateController,
        _onEventController,
        _onChangeController,
        _onTransitionController,
        _onErrorController,
        _onCloseController
      ];

  @override
  @mustCallSuper
  void onCreate(Cubit cubit) {
    super.onCreate(cubit);
    _onCreateController.sink.add(const CubitCreate());
  }

  @override
  @mustCallSuper
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    _onEventController.sink.add(BlocEvent(bloc: bloc, event: event));
  }

  @override
  @mustCallSuper
  void onChange(Cubit cubit, Change change) {
    super.onChange(cubit, change);
    _onChangeController.sink.add(CubitChange(cubit: cubit, change: change));
  }

  @override
  @mustCallSuper
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    _onTransitionController.sink
        .add(BlocTransition(bloc: bloc, transition: transition));
  }

  @override
  @mustCallSuper
  void onError(Cubit cubit, Object error, StackTrace stackTrace) {
    super.onError(cubit, error, stackTrace);
    _onErrorController.sink
        .add(CubitError(cubit: cubit, error: error, stackTrace: stackTrace));
  }

  @override
  @mustCallSuper
  void onClose(Cubit cubit) {
    super.onClose(cubit);
    _onCloseController.sink.add(const CubitClose());
  }

  @mustCallSuper
  void dispose() {
    _controllers.forEach((controller) => controller.close());
  }
}
