import 'package:freezed_annotation/freezed_annotation.dart';

part 'observer_state.freezed.dart';

@freezed
class ObserverState with _$ObserverState {
  const ObserverState._();

  // TODO: Add wrap-states here
  const factory ObserverState.auth(/*{required AuthState state}*/) =
      AuthObserverState;
}
