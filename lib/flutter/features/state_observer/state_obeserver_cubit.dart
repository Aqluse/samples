import 'dart:async';

import 'observer_state.dart';
import 'state_observer_cubit_base.dart';
import 'stream_bloc_observer.dart';

class StateObserverCubit extends StateObserverCubitBase<ObserverState> {
  StateObserverCubit({required Stream<CubitChange> changeStream})
      : super(changeStream: changeStream);

  @override
  FutureOr<ObserverState?> mapChangeToState(CubitChange change) {
    final cubit = change.cubit;

    // TODO: Conversion example
    /*if (cubit is ChatMessagesBloc)
      return ObserverState.chat(
          state: change.change.nextState as ChatMessagesState);
    else*/
    return null;
  }
}
