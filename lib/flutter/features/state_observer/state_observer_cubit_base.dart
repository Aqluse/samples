import 'dart:async';

import 'package:bloc/bloc.dart';

import 'stream_bloc_observer.dart';

abstract class StateObserverCubitBase<T> extends Cubit<T> {
  StateObserverCubitBase({required Stream<CubitChange> changeStream})
      : super(null) {
    changeStreamSub = changeStream
        .where((change) => !_selfEmittedState(change.cubit))
        .listen(_emitter);
  }

  late final StreamSubscription<CubitChange> changeStreamSub;

  @override
  Future<void> close() {
    changeStreamSub.cancel();
    return super.close();
  }

  // ignore: avoid_void_async
  void _emitter(CubitChange change) async {
    final observerState = await mapChangeToState(change);
    if (observerState != null) emit(observerState);
  }

  FutureOr<T?> mapChangeToState(CubitChange change);

  bool _selfEmittedState(Cubit cubit) {
    return cubit.runtimeType == this.runtimeType;
  }
}
