// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'observer_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ObserverStateTearOff {
  const _$ObserverStateTearOff();

  AuthObserverState auth() {
    return const AuthObserverState();
  }
}

/// @nodoc
const $ObserverState = _$ObserverStateTearOff();

/// @nodoc
mixin _$ObserverState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() auth,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? auth,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthObserverState value) auth,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthObserverState value)? auth,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ObserverStateCopyWith<$Res> {
  factory $ObserverStateCopyWith(
          ObserverState value, $Res Function(ObserverState) then) =
      _$ObserverStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ObserverStateCopyWithImpl<$Res>
    implements $ObserverStateCopyWith<$Res> {
  _$ObserverStateCopyWithImpl(this._value, this._then);

  final ObserverState _value;
  // ignore: unused_field
  final $Res Function(ObserverState) _then;
}

/// @nodoc
abstract class $AuthObserverStateCopyWith<$Res> {
  factory $AuthObserverStateCopyWith(
          AuthObserverState value, $Res Function(AuthObserverState) then) =
      _$AuthObserverStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthObserverStateCopyWithImpl<$Res>
    extends _$ObserverStateCopyWithImpl<$Res>
    implements $AuthObserverStateCopyWith<$Res> {
  _$AuthObserverStateCopyWithImpl(
      AuthObserverState _value, $Res Function(AuthObserverState) _then)
      : super(_value, (v) => _then(v as AuthObserverState));

  @override
  AuthObserverState get _value => super._value as AuthObserverState;
}

/// @nodoc
class _$AuthObserverState extends AuthObserverState {
  const _$AuthObserverState() : super._();

  @override
  String toString() {
    return 'ObserverState.auth()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is AuthObserverState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() auth,
  }) {
    return auth();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? auth,
    required TResult orElse(),
  }) {
    if (auth != null) {
      return auth();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthObserverState value) auth,
  }) {
    return auth(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthObserverState value)? auth,
    required TResult orElse(),
  }) {
    if (auth != null) {
      return auth(this);
    }
    return orElse();
  }
}

abstract class AuthObserverState extends ObserverState {
  const factory AuthObserverState() = _$AuthObserverState;
  const AuthObserverState._() : super._();
}
