import 'dart:async';

import 'package:flutter/widgets.dart';

typedef StreamListenerFilter<T> = bool Function(T? previous, T current);
typedef StreamListenerFunc<T> = void Function(T? previous, T current);
typedef StreamBuilderFunc<T> = Widget Function(
    BuildContext context, T? value, Widget? child);

class StreamListenerBuilder<T> extends StatefulWidget {
  const StreamListenerBuilder(
      {Key? key,
      required this.stream,
      this.initialData,
      this.when,
      this.listenWhen,
      this.listener,
      this.buildWhen,
      this.builder,
      this.child})
      : assert(
            listener != null || builder != null,
            'Neither `listener` nor `builder` provided to the widget. '
            "Don't you forget about them?"),
        assert(
            when == null || listenWhen == null && buildWhen == null,
            'Providing `listenWhen` and `buildWhen` while `when` is also used '
            'is a possible mistake. '
            '`when` will given priority above other filters'),
        assert(listenWhen == null || listener != null,
            'Probably forgot to provide `listener`'),
        assert(buildWhen == null || builder != null,
            'Probably forgot to provide `builder`'),
        super(key: key);

  final Stream<T> stream;
  final T? initialData;

  final StreamListenerFilter<T>? when;

  final StreamListenerFilter<T>? listenWhen;
  final StreamListenerFunc<T>? listener;

  // TODO: value заменить на AsyncSnapshot
  final StreamListenerFilter<T>? buildWhen;
  final StreamBuilderFunc<T>? builder;

  final Widget? child;

  @override
  _StreamListenerBuilderState<T> createState() =>
      _StreamListenerBuilderState<T>();
}

class _StreamListenerBuilderState<T> extends State<StreamListenerBuilder<T>> {
  // ignore: cancel_subscriptions
  StreamSubscription<T>? _subscription;
  StreamSubscription<T>? _oldSubscription;
  T? _listenerValue;
  T? _builderValue;

  @override
  void initState() {
    super.initState();
    _initValues();
    _sub();
  }

  @override
  void dispose() {
    _shelveSubscription();
    _unsub();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant StreamListenerBuilder<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.stream != widget.stream) {
      _shelveSubscription();
      _sub();
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder?.call(context, _builderValue, widget.child) ??
        widget.child!;
  }

  void _sub() {
    _subscription = widget.stream.listen((value) {
      _unsub();

      final satisfiesWhen = widget.when?.call(_listenerValue, value);
      if (widget.listener != null &&
          (satisfiesWhen ??
              widget.listenWhen?.call(_listenerValue, value) ??
              true)) widget.listener!(_listenerValue, _listenerValue = value);
      if (widget.builder != null &&
          (satisfiesWhen ??
              widget.buildWhen?.call(_builderValue, value) ??
              true)) setState(() => _builderValue = value);
    });
  }

  void _shelveSubscription() {
    _oldSubscription = _subscription;
  }

  void _unsub() {
    _oldSubscription?.cancel();
    _oldSubscription = null;
  }

  void _initValues() {
    // TODO: А фильтр применить?
    _listenerValue = widget.initialData;
    _builderValue = widget.initialData;
  }
}
