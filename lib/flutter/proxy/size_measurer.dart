import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';

enum MeasureMethod { renderObject, layoutBuilder }

typedef ConstraintListener = void Function(BoxConstraints constraints);
typedef NullableConstraintBuilder = Widget Function(
    BuildContext context,
    BoxConstraints? constraints,
    Widget? child
);
typedef ConstraintBuilder = Widget Function(
    BuildContext context,
    BoxConstraints constraints,
    Widget? child
);

/// Provides its size as soon as layout is ready
class SizeMeasurer extends StatefulWidget {

  const SizeMeasurer._(this.measureMethod, {
    this.listener,
    this.builder,
    this.child
  }) : assert(builder != null || child != null);

  /// Size will be provided as a maximum span constraint
  /// Initial constraints always will be `null`
  const SizeMeasurer.renderObject({
    ConstraintListener? listener,
    NullableConstraintBuilder? builder,
    Widget? child
  }) : this._(
      MeasureMethod.renderObject,
      listener: listener,
      builder: builder,
      child: child
  );

  /// Conflicts with return of intrinsic dimensions to parent widgets
  /// Provides non-null constraints
  const SizeMeasurer.layoutBuilder({
    ConstraintListener? listener,
    ConstraintBuilder? builder,
    Widget? child
  }) : this._(
      MeasureMethod.layoutBuilder,
      listener: listener,
      builder: builder,
      child: child
  );

  /// Way that used to measure a widget size
  final MeasureMethod measureMethod;

  /// Introduces another way to deliver measured size
  final ConstraintListener? listener;
  /// Provides size with an optional [child]-widget,
  /// to give a direct access to measured constraints size within a widget tree
  final Function? builder;

  final Widget? child;

  @override
  _SizeMeasurerState createState() => _SizeMeasurerState();
}

class _SizeMeasurerState extends State<SizeMeasurer> {

  BoxConstraints? _lastConstraint;

  @override
  Widget build(BuildContext context) {
    if (widget.measureMethod == MeasureMethod.renderObject) {
      SchedulerBinding.instance!.addPostFrameCallback((_) {
        final size = (context.findRenderObject() as RenderBox).size;
        final constraints = BoxConstraints(
            maxHeight: size.height,
            maxWidth: size.width
        );
        setState(() => _lastConstraint = constraints);
      });
      return _refreshConstraints(constraints: _lastConstraint);
    }
    else if (widget.measureMethod == MeasureMethod.layoutBuilder)
      return LayoutBuilder(
        builder: (context, constraints) {
          return _refreshConstraints(constraints: constraints);
        },
      );
    throw UnimplementedError();
  }

  Widget _refreshConstraints({ required BoxConstraints? constraints }) {
    if (constraints != null) {
      widget.listener?.call(constraints);
      SizeNotification(constraints).dispatch(context);
    }
    final builder = widget.builder;
    return builder == null
        ? widget.child!
        : builder is NullableConstraintBuilder
        ? builder.call(context, constraints, widget.child)
        : builder is ConstraintBuilder && constraints != null
        ? builder.call(context, constraints, widget.child)
        : throw UnimplementedError();
  }
}

/// Notification with widget size to bubble-up within a tree
class SizeNotification extends Notification {

  final BoxConstraints constraints;

  const SizeNotification(this.constraints);
}