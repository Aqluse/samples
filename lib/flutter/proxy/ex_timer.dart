// @dart=2.10

import 'dart:async';

import 'package:flutter/widgets.dart';

enum TimerType { countdown, stopWatch }

typedef OnDurationCallback = void Function(Duration value);
typedef DurationChildBuilder = Widget Function(
    BuildContext context,
    /// Can possibly be `null`
    Widget child,
    Duration value);


class ExTimer extends StatefulWidget {

  const ExTimer._({
    @required this.type,
    ExTimerController controller,
    bool manualStart,
    Duration updateRate,
    this.startsFrom,
    this.stopBarrier,
    this.onUpdate,
    this.onFinished,
    this.childBuilder,
    this.child
  }) : assert(type != null),
        assert(type != TimerType.countdown
            || startsFrom != null),
        assert(childBuilder != null
            || child != null),
        _controller = controller,
        manualStart = manualStart
            ?? false,
        updateRate = updateRate
            ?? const Duration(seconds: 1);

  const ExTimer.countdown({
    ExTimerController controller,
    bool manualStart,
    Duration updateRate,
    @required Duration startFrom,
    Duration stopBarrier,
    OnDurationCallback onUpdate,
    OnDurationCallback onFinished,
    DurationChildBuilder childBuilder,
    Widget child
  }) : this._(
      controller: controller,
      manualStart: manualStart,
      type: TimerType.countdown,
      updateRate: updateRate,
      startsFrom: startFrom,
      stopBarrier: stopBarrier
          ?? const Duration(seconds: 0),
      onUpdate: onUpdate,
      onFinished: onFinished,
      childBuilder: childBuilder,
      child: child
  );

  const ExTimer.stopWatch({
    ExTimerController controller,
    bool manualStart,
    Duration updateRate,
    Duration startFrom,
    Duration stopBarrier,
    OnDurationCallback onUpdate,
    OnDurationCallback onFinished,
    DurationChildBuilder childBuilder,
    Widget child
  }) : this._(
      controller: controller,
      manualStart: manualStart,
      type: TimerType.stopWatch,
      updateRate: updateRate,
      startsFrom: startFrom
          ?? const Duration(seconds: 0),
      stopBarrier: stopBarrier,
      onUpdate: onUpdate,
      onFinished: onFinished,
      childBuilder: childBuilder,
      child: child
  );

  final TimerType type;
  final ExTimerController _controller;
  /// Whether should start automatically
  /// or [ExTimerController] must be called with [ExTimerController.start] explicitly
  final bool manualStart;
  /// Frequency with which value of timer to be updated
  final Duration updateRate;
  /// Initial timestamp of a timer
  final Duration startsFrom;
  /// If timer value reaches this value,
  /// then [onFinished] callback will be executed
  final Duration stopBarrier;

  final OnDurationCallback onUpdate;
  /// When reaches [stopBarrier] or timer has been cancelled in any possible way
  final OnDurationCallback onFinished;

  final DurationChildBuilder childBuilder;
  final Widget child;

  @override
  _ExTimerState createState() => _ExTimerState();

}

class _ExTimerState extends State<ExTimer> {

  ExTimerController _controller;

  @override
  void initState() {
    super.initState();
    _initController();
  }

  @override
  void didUpdateWidget(covariant ExTimer oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget._controller != oldWidget._controller)
      _initController();
  }

  @override
  void dispose() {
    _controller
      ..removeListener(_onUpdate)
    /*..dispose()*/; // TODO: Падают ошибки
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final child = widget.child;
    return ExTimerScope(
      controller: _controller,
      child: widget.childBuilder
          ?.call(context, child, _controller.value)
          ?? child,
    );
  }

  void _initController() {
    _controller = widget._controller
        ?? ExTimerController();
    _controller
      ..attach(widget)
      ..addListener(_onUpdate);

  }

  void _onUpdate() {
    if (widget.childBuilder != null)
      setState(() {});
  }

}

class ExTimerController extends ValueNotifier<Duration> {

  // Use getter to get value of this field
  ExTimer _hostValue;
  ExTimer get _host => _hostValue != null
      ? _hostValue
      : throw Exception('Cannot use controller without any host attached. '
      'Consider use of widget that will do that for you (ExTimer) implicitly '
      'or set host with `attach()` method');

  Timer _timer;

  ExTimerController()
      : super(Duration.zero);

  void start() {
    if (_timer != null)
      return;

    if (_shouldStopTimer())
      stop();
    _timer = Timer.periodic(_host.updateRate, (timer) {
      switch (_host.type) {
        case TimerType.countdown:
          value -= _host.updateRate;
          _host.onUpdate?.call(value);
          break;
        case TimerType.stopWatch:
          value += _host.updateRate;
          _host.onUpdate?.call(value);
          break;
      }
      if (_shouldStopTimer())
        stop();
    });
  }

  void pause() {
    _timer?.cancel();
    _timer = null;
  }

  Future<void> stop() async {
    pause();
    return _host.onFinished?.call(value);
  }

  Future<void> reset() async {
    return _initialize();
  }

  Future<void> attach(ExTimer timer) async {
    assert(timer != null);
    _hostValue = timer;
    return _initialize();
  }

  Future<void> detach() async {
    return _hostValue = null;
  }

  @override
  Future<void> dispose() async {
    _timer?.cancel();
    await detach();
    super.dispose();
  }

  void _initialize() {
    value = _host.startsFrom;
    if (!_host.manualStart)
      start();
  }

  bool _shouldStopTimer() {
    bool shouldStop = false;
    if (_host.stopBarrier != null)
      switch (_host.type) {
        case TimerType.countdown:
          if (value <= _host.stopBarrier)
            shouldStop = true;
          break;
        case TimerType.stopWatch:
          if (value >= _host.stopBarrier)
            shouldStop = true;
          break;
      }
    return shouldStop;
  }

}

class ExTimerScope extends InheritedWidget {

  final ExTimerController controller;

  const ExTimerScope({
    @required this.controller,
    @required Widget child
  }) : super(child: child);

  ExTimerScope of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<ExTimerScope>();

  @override
  bool updateShouldNotify(ExTimerScope oldWidget) {
    return controller != oldWidget.controller;
  }

}