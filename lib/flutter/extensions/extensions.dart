extension WidgetListExtensions<T> on List<T> {
  /// Inserts separator AFTER each [List] element, thus skips last one
  void insertSeparator(T separator) {
    // Note that the index means where separator to-be inserted,
    // and not the index of original list
    for (int i = this.length - 1; i > 0; i--) this.insert(i, separator);
  }
}
