import 'package:flutter/widgets.dart';

// TODO: Что виджет делает?
class ExpandedChildScrollView extends StatelessWidget {
  const ExpandedChildScrollView({Key? key, required this.child}) : super(key: key);

  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(minHeight: constraints.maxHeight),
                child: IntrinsicHeight(
                  // TODO: Закомментировать что делает этот intrinsic (нет, не для Center внутри)
                  child: child,
                ),
              ),
            ));
  }
}
