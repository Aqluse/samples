import 'package:flutter/widgets.dart';

class OverflowText extends StatelessWidget {
  const OverflowText({
    final Key? key,
    required this.text,
    required this.textPainter,
    required this.visibleBuilder,
    required this.collapsedBuilder,
  }) : super(key: key);

  final Text text;
  final TextPainter textPainter;
  final Text Function(BuildContext context, Text text) visibleBuilder;
  final Text Function(BuildContext context, Text text) collapsedBuilder;

  @override
  Widget build(final BuildContext context) {
    return LayoutBuilder(
      builder: (final context, final constraints) {
        textPainter.layout(maxWidth: constraints.maxWidth);
        if (textPainter.didExceedMaxLines) {
          return collapsedBuilder(context, text);
        } else {
          return visibleBuilder(context, text);
        }
      },
    );
  }
}

/// Example of use
/// Widget build() {
///   // It is necessary to set same `maxLines` and `style` to the both TextPainter and Text
///   const maxLines = 4;
///   final textStyle = AppTextStyles.textStyle;
///   return OverflowText(
///         text: Text(text,
///             maxLines: maxLines,
///             overflow: TextOverflow.ellipsis,
///             style: textStyle
///         ),
///         textPainter: TextPainter(
///             text: TextSpan(
///                 text: text,
///                 style: textStyle
///             ),
///             maxLines: maxLines,
///             textDirection: TextDirection.ltr
///         ),
///         collapsedBuilder: (context, text) =>
///             Column(
///                 crossAxisAlignment: CrossAxisAlignment.stretch,
///                 children: [
///                   text,
///                   Text.rich(
///                       TextSpan(
///                           text:'Expand',
///                           style: AppTextStyles.secondaryTextButton,
///                           recognizer: TapGestureRecognizer()..onTap = () => null
///                       )
///                   )
///                 ]..insertSeparator(SizedBox(height: 8))
///             )
///         ,
///         visibleBuilder: (context, text) => text
///     );
/// }
