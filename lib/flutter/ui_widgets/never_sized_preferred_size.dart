import 'package:flutter/widgets.dart';

import '../proxy/size_measurer.dart';

/// The purpose of this widget to avoid aggressive guidance to use, as an example,
/// constant-sized [PreferredSize] within bottom of [SliverAppBar]. So it's a light workaround.
class NeverSizedPreferredSize extends StatefulWidget {

  const NeverSizedPreferredSize({
    Key? key,
    required this.child
  }) : super(key: key);

  final Widget child;

  @override
  _NeverSizedPreferredSizeState createState() => _NeverSizedPreferredSizeState();
}

class _NeverSizedPreferredSizeState extends State<NeverSizedPreferredSize> {

  late BoxConstraints _constraints;

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(_constraints.maxHeight),
      child: UnconstrainedBox(
        constrainedAxis: Axis.horizontal,
        child: SizeMeasurer.layoutBuilder( // TODO: Этот конструктор может не сработать
          listener: (constraints) => setState(() => _constraints = constraints),
          child: widget.child
        )
      ),
    );
  }
}
